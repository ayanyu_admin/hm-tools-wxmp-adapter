package top.hmtools.wxmp.core;

import top.hmtools.wxmp.core.configuration.WxmpConfiguration;

/**
 * 缺省的微信公众号会话工厂
 * @author HyboWork
 *
 */
public class DefaultWxmpSessionFactory extends WxmpSessionFactory {

	public DefaultWxmpSessionFactory(WxmpConfiguration configuration) {
		super(configuration);
	}

	@Override
	public WxmpSession openSession() {
		DefaultWxmpSession result = new DefaultWxmpSession(this.getConfiguration());
		return result;
	}

}
