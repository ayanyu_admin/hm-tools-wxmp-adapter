package top.hmtools.wxmp.core.configuration;

import java.io.Serializable;

import top.hmtools.wxmp.core.WxmpSessionFactory;
import top.hmtools.wxmp.core.access_handle.BaseAccessTokenHandle;

public class WxmpConfiguration implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7069735246475567822L;

	
	
	/**
	 * 收、发数据时的字符编码，缺省 utf-8
	 */
	private String charset;
	
	/**
	 * 强制使用的accessToken字符串
	 * <br>当本属性被设置为不为空，且不为空字符串时，则强制使用该accessToken字符串，以便于使用某些场景
	 */
	private String forceAccessTokenString;

	/**
	 * 微信服务器地址
	 */
	private EUrlServer eUrlServer;
	
	/**
	 * 存放微信公众号appid与appsecret数据对的盒子
	 */
//	private AppIdSecretBox appIdSecretBox;
	
	/**
	 * 获取access token的中间件
	 */
	private BaseAccessTokenHandle accessTokenHandle;
	
	/**
	 * 会话工厂
	 */
	private WxmpSessionFactory wxmpSessionFactory ;

	public String getCharset() {
		if(this.charset == null || this.charset.trim().length()<1){
			this.charset = "utf-8";
		}
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getForceAccessTokenString() {
		return forceAccessTokenString;
	}

	public void setForceAccessTokenString(String forceAccessTokenString) {
		this.forceAccessTokenString = forceAccessTokenString;
	}

	public EUrlServer geteUrlServer() {
		if(this.eUrlServer == null){
			this.eUrlServer = EUrlServer.api;
		}
		return eUrlServer;
	}

	public void seteUrlServer(EUrlServer eUrlServer) {
		this.eUrlServer = eUrlServer;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public WxmpSessionFactory getWxmpSessionFactory() {
		return wxmpSessionFactory;
	}

	public void setWxmpSessionFactory(WxmpSessionFactory wxmpSessionFactory) {
		this.wxmpSessionFactory = wxmpSessionFactory;
	}

	public BaseAccessTokenHandle getAccessTokenHandle() {
		return accessTokenHandle;
	}

	public void setAccessTokenHandle(BaseAccessTokenHandle accessTokenHandle) {
		this.accessTokenHandle = accessTokenHandle;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
