package top.hmtools.wxmp.message.ordinary.model;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.annotation.WxmpMessage;
import top.hmtools.wxmp.core.model.message.BaseMessage;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * 地理位置消息
 * {@code
 * <xml>
	<ToUserName><![CDATA[toUser]]></ToUserName>
	<FromUserName><![CDATA[fromUser]]></FromUserName>
	<CreateTime>1351776360</CreateTime>
	<MsgType><![CDATA[location]]></MsgType>
	<Location_X>23.134521</Location_X>
	<Location_Y>113.358803</Location_Y>
	<Scale>20</Scale>
	<Label><![CDATA[位置信息]]></Label>
	<MsgId>1234567890123456</MsgId>
</xml>
 * }
 * @author HyboWork
 *
 */
@WxmpMessage(msgType=MsgType.location)
public class LocationMessage extends BaseMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6815992243843473109L;
	
	/**
	 * 地理位置维度
	 */
	@XStreamAlias("Location_X")
	private String location_X;
	
	/**
	 * 地理位置经度
	 */
	@XStreamAlias("Location_Y")
	private String location_Y;
	
	/**
	 * 地图缩放大小
	 */
	@XStreamAlias("Scale")
	private String scale;
	
	/**
	 * 地理位置信息
	 */
	@XStreamAlias("Label")
	private String label;

	public String getLocation_X() {
		return location_X;
	}

	public void setLocation_X(String location_X) {
		this.location_X = location_X;
	}

	public String getLocation_Y() {
		return location_Y;
	}

	public void setLocation_Y(String location_Y) {
		this.location_Y = location_Y;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public void configXStream(XStream xStream) {
		
	}

	@Override
	public String toString() {
		return "LocationMessage [location_X=" + location_X + ", location_Y=" + location_Y + ", scale=" + scale
				+ ", label=" + label + ", toUserName=" + toUserName + ", fromUserName=" + fromUserName + ", createTime="
				+ createTime + ", msgType=" + msgType + ", msgId=" + msgId + "]";
	}

	
}
