package top.hmtools.wxmp.message.group.model;

/**
 * Auto-generated: 2019-08-27 17:28:34
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class DeleteParam {

    private int msg_id;
    private int article_idx;
    public void setMsg_id(int msg_id) {
         this.msg_id = msg_id;
     }
     public int getMsg_id() {
         return msg_id;
     }

    public void setArticle_idx(int article_idx) {
         this.article_idx = article_idx;
     }
     public int getArticle_idx() {
         return article_idx;
     }

}