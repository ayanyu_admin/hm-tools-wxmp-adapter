[![Maven Central](https://img.shields.io/maven-central/v/top.hmtools/wxmp-account.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22top.hmtools%22%20AND%20a:%22wxmp-account%22)
#### 前言
本组件对应实现微信公众平台“账号管理”章节相关api接口，原接口文档地址：[账号管理](https://developers.weixin.qq.com/doc/offiaccount/Account_Management/Generating_a_Parametric_QR_Code.html)

#### 接口说明
- `top.hmtools.wxmp.account.apis.IQrcodeApis` 对应实现“带参数的二维码”相关接口。
- `top.hmtools.wxmp.account.apis.IShortUrlApis` 对应实现“长链接转短链接接口”相关接口。

#### 事件消息类说明
- `top.hmtools.wxmp.account.models.eventMessage`包下对应实现“微信认证事件推送”相关xml数据对应的Javabean数据结构。可参阅 top.hmtools.wxmp.account.enums.EQRActionName ， top.hmtools.wxmp.account.enums.EVerifyEventMessages

#### 使用示例
0. 引用jar包
```
<dependency>
  <groupId>top.hmtools</groupId>
  <artifactId>wxmp-account</artifactId>
  <version>1.0.0</version>
</dependency>
```

1. 获取wxmpSession，参照 [wxmp-core/readme.md](../wxmp-core/readme.md)
```
//2 获取动态代理对象实例
IQrcodeApis qrcodeApis = this.wxmpSession.getMapper(IQrcodeApis.class);

//3 创建二维码ticket
QRCodeParam codeParam = new QRCodeParam();

ActionInfo action_info = new ActionInfo();
Scene scene = new Scene();
scene.setScene_str("aaaabbbbcccc");
action_info.setScene(scene);
codeParam.setAction_info(action_info);

codeParam.setAction_name(EQRActionName.QR_STR_SCENE);

codeParam.setExpire_seconds(20*60*60*60L);
QRCodeResult create = this.qrcodeApis.create(codeParam);
```

更多示例参见：
- [带参数的二维码](src/test/java/top/hmtools/wxmp/account/apis/IQrcodeApisTest.java)
- [长链接转短链接接口](src/test/java/top/hmtools/wxmp/account/apis/IShortUrlApisTest.java)
- [微信认证事件推送](src/test/java/top/hmtools/wxmp/account/message/WxmpMessageTest.java)

2020.06.01		账号管理组件全部测试 OK
*top.hmtools.wxmp.account.enums.EVerifyEventMessagesTest 中使用了自动填充Javabean数据工具*
